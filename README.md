# Cutting Shears

![](shears.PNG)

The cutting Shears consist of different parts that are welded and screwed together.


The operation of the hand lever Shears is particularly simple and energy-saving. The extra long handle and the resulting leverage enable effortless cutting of even thicker raw materials. The non-slip rubber handle guarantees a comfortable and accurate way of working.


Stability when cutting the materials is guaranteed by a fixed installation via the corresponding recesses on the leg of the device. The holes guarantee quick and secure attachment and also allow uncomplicated dismantling and thus mobile use of the tool on various construction sites.

